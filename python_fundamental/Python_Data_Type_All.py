print()
print(" ------------------------------------- PYTHON DATA TYPE------------------------------------------")
print()
data_type_indro = ''' Data Type represents the type of data present inside a variable.
In Python we are not required to specify the type explicitly. Based on value provided,
the type will be assigned automatically.Hence Python is dynamically Typed Language
 '''
print(data_type_indro)

data_type_list = """ Python contains the following inbuilt data types

1) Int 
2) Float 
3) Complex 
4) Bool 
5) Str
6) Bytes
7) Bytearray 
8) Range 
9) List
10)Tuple 
11) Set
12) Frozenset 
13) Dict
14) None
 """
print(data_type_list)

 
fun_check_dt =''' Python Inbuild Function :
type() to check the type of variable
id() to get address of object
 '''
print(fun_check_dt)
 
print(" :::::::::::::::::::::: INT DATA TYPE ::::::::::::::::::")
int_data_type = ''' 1) int Data Type:
We can use int data type to represent whole numbers (integral values)

number = 101010
print(type(number))#<class 'int'>
print(number) #101010
:::OUTPUT::: '''
print(int_data_type) 
 
number = 101010
print(type(number))#<class 'int'>
print(number) #101010
print()

int_repre = """ We can represent int values in the following ways
1) Decimal form
2) Binary form 
3) Octal form 
4) Hexa decimal form
 """
print(int_repre)

decimal_form = '''
1) Decimal Form (Base-10):
It is the default number system in Python
The allowed digits are: 0 to 9
Ex: 
dec_form = 0
print(dec_form)#0
dec_form = 12121212
print(type(dec_form),dec_form)#<class 'int'> 12121212 
:: OUTPUT:: '''
print(decimal_form)
 
dec_form = 0
print(dec_form)#0
dec_form = 12121212
print(type(dec_form),dec_form)#<class 'int'> 12121212 


binary_form = ''' 
2) Binary Form (Base-2):
The allowed digits are : 0 & 1 
bin_form = 0b012 #SyntaxError: invalid digit '2' in binary literal 
Literal value should be prefixed with 0b or 0B
Ex: 
bin_form = 0b10101
print(bin_form)
:: OUTPUT :: '''
print(binary_form)

bin_form = 0b0101010
print(bin_form)#42

#bin_form = 0b012 #SyntaxError: invalid digit '2' in binary literal 

octal_form = '''
3) Octal Form (Base-8):
The allowed digits are : 0 to 7
#octal_form = 0O78464 #SyntaxError: invalid digit '8' in octal literal
Literal value should be prefixed with 0o or 0O.
Ex: 
octal_form = 0o123456
print(octal_form)# 42798
:: OUTPUT :: 
'''
print(octal_form)

octal_form = 0o123456
print(octal_form)# 42798
#octal_form = 0O78464 #SyntaxError: invalid digit '8' in octal literal


hexa_d_form = ''' 
4) Hexa Decimal Form (Base-16):
The allowed digits are: 0 to 9, a-f (both lower and upper cases are allowed) 
Literal value should be prefixed with 0x or 0X
Ex:
hexa_d_form1 = 0xABCDEF
hexa_d_form2 = 0X787169
print(hexa_d_form1,'::',hexa_d_form2)
:: OUTPUT :: '''
print(hexa_d_form)

hexa_d_form1 = 0xABCDEF
hexa_d_form2 = 0X787169
print(hexa_d_form1,'::',hexa_d_form2)

print()
print(" ::: Being a programmer we can specify literal values in decimal, binary, octal and hexa decimal forms"
      "But PVM will always provide values only in decimal form :::")

print(" :::::::::::::::::::::::::::::::::::::::::::: FLOAT DATA TYPE :::::::::::::::::::::::::::::::::::::::::")

float_data_type = ''' 
We can use float data type to represent floating point values (decimal values) 
Eg: f = 1.234
type(f) float
We can also represent floating point values by using exponential form (Scientific Notation)
Eg: f = 1.2e3 ... instead of 'e' we can use 'E' 
print(f) 1200.0
The main advantage of exponential form is we can represent big values in less '''
print(float_data_type)
print()

float_value = 10101.111
print(float_value)

float_val_rep = '''
NOTE: We can represent int values in decimal, binary, octal and hexa decimal forms. But we can represent float values only by using decimal form::
#float_value = 0b101.10 #SyntaxError: invalid syntax
#float_value = 0o175.85 #SyntaxError: invalid syntax
#float_value = 0X787169.54 #SyntaxError: invalid syntax
'''
print(float_val_rep)

print(" :::::::::::::::::::::::::::::::::::::::::::: COMPLEX DATA TYPE :::::::::::::::::::::::::::::::::::::::::")

complex_data_type = ''' 
A complex number is of the form a+bj 
a = real part b = imaginary part 
‘a’ and ‘b’ contain Intergers OR Floating Point Values.
Eg: complex_data_type = 10 + 10j
complex_data_type = 10.0 + 10.5j
complex_data_type = 0b101 + 10j
complex_data_type = 0o157 + 464j
complex_data_type = 0x1281 + 4646546j
:: OUTPUT ::'''
print(complex_data_type)

complex_data_type = 10 + 10j
complex_data_type = 10.0 + 10.5j
complex_data_type = 0b101 + 10j
complex_data_type = 0o157 + 464j
complex_data_type = 0x1281 + 4646546j
print(complex_data_type)

notes = '''In the real part if we use int value then we can specify that either by decimal, octal, binary or hexa decimal form.
But imaginary part should be specified only by using decimal form 
#EX : complex_data_type = 0x1281 + 0o157j#SyntaxError: invalid syntax
# '''
print(notes)

#complex_data_type = 0x1281 + 0o157j#SyntaxError: invalid syntax

complex_attr = '''
Note: Complex data type has some inbuilt attributes to retrieve the real part and imaginary part
c = 10.5+3.6j
c.real:10.5 
c.imag : 3.6 '''
print(complex_attr)

complex_data_type = 0x1281 + 4646546j
print(complex_data_type.real)#4737.0
print(complex_data_type.imag)#4646546.0


print(" :::::::::::::::::::::::::::::::::::::::::::: BOOL DATA TYPE :::::::::::::::::::::::::::::::::::::::::")

bool_data_type = '''
We can use this data type to represent boolean values.
 The only allowed values for this data type are:  True and False
 Internally Python represents True as 1 and False as 0

>>> bool_type = True
>>> type(bool_type)
<class 'bool'>

>>> bool_type = False
>>> type(bool_type)
<class 'bool'>

>>> bool_type = 0
>>> type(bool_type)
<class 'int'>
'''
print(bool_data_type)

bool_data_type = True
print(type(bool_data_type))
print(bool_data_type) 

print(" :::::::::::::::::::::::::::::::::::::::::::: str (STRING) DATA TYPE :::::::::::::::::::::::::::::::::::::::::")

string_data_type = '''
str represents String data type.
A String is a sequence of characters enclosed within single quotes ' ' or double quotes " "
'''
print(string_data_type)

string_data_type1 = 'Hello Welcome To Python this is single line string '
string_data_type2 = "Hello Welcome Samadhan this is single line string "
string_data_type = """ Hello Welcome This is Multine String 
                       'single quotes' or "double quotes" we cannot represent 
                        multi line string literals 
                        For this requirement we should go for 
                        triple single quotes (''') or triple double quotes (" " ") 
                        """
print(string_data_type1)
print(string_data_type2)
print(string_data_type)


print(" :::::::::::::::::::::::::::::::::::::::::::: bytes DATA TYPE :::::::::::::::::::::::::::::::::::::::::")

bytes_data_type = ''' 
bytes data type represens a group of byte numbers just like an array.
The only allowed values for byte data type are 0 to 256. 
By mistake if we are trying to provide any other values then we will get value error
#EX: data = [10,20,True,101,260]#ValueError: bytes must be in range(0, 256)

Ex: data = [10,20,True,101,200]
print(type(data),data)
bytes_data_type = bytes(data)
print(type(bytes_data_type),bytes_data_type)
:: OUTPUT :: 
'''
print(bytes_data_type)

#data = [10, 20, 'Samadhan', 10.5, True, 'OK', 101]
#TypeError: 'str' & Float  object cannot be interpreted as an integer

#data = [10,20,True,101,260]#ValueError: bytes must be in range(0, 256)

data = [10,20,True,101,200]
print(type(data),data)
bytes_data_type = bytes(data)
print(type(bytes_data_type),bytes_data_type)
print(bytes_data_type[0])#10

#bytes_data_type[0]=78
#TypeError: 'bytes' object does not support item assignment

for i in bytes_data_type:
    print(i)
    
''' OUTPUT: 
10
20
1
101
200
'''
note ='''
Once we creates bytes data type value, we cannot change its values,
otherwise we will get TypeError

#bytes_data_type[0]=78
#TypeError: 'bytes' object does not support item assignment
'''
print(note)

print(" :::::::::::::::::::::::::::::::::::::::::::: bytearray DATA TYPE :::::::::::::::::::::::::::::::::::::::::")

bytearray_data_type = ''' 
bytearray is exactly same as bytes data type except that its elements can be modified

bytearray data type represens a group of byte numbers just like an array.
The only allowed values for byte data type are 0 to 256. 
By mistake if we are trying to provide any other values then we will get value error
#EX: data = [10,20,True,101,260]#ValueError: bytes must be in range(0, 256)

Ex: data = [10,20,True,101,200]
print(type(data),data)
bytearray_data_type = bytearray(data)
print(type(bytearray_data_type),bytearray_data_type)
:: OUTPUT :: 
'''
print(bytearray_data_type)

#data = [10, 20, 'Samadhan', 10.5, True, 'OK', 101]
#TypeError: 'str' & Float  object cannot be interpreted as an integer

#data = [10,20,True,101,260]#ValueError: bytes must be in range(0, 256)

data = [10,20,True,101,200]
print(type(data),data)
bytearray_data_type = bytearray(data)
print(type(bytearray_data_type),bytearray_data_type)
for i in bytearray_data_type:
    print(i)
    
''' OUTPUT: 
10
20
1
101
200
'''
note = '''
bytesarray elements can be modified
Ex: bytearray_data_type[0]=100
'''
print(note)

print("elements before modified",bytearray_data_type[0])#10 
bytearray_data_type[0]=100
print("elements after  modified",bytearray_data_type[0])

print(" :::::::::::::::::::::::::::::::::::::::::::: bytes DATA TYPE :::::::::::::::::::::::::::::::::::::::::")


list_data_type = '''
List Data Type: 
֍ Ordered, mutable, heterogenous collection of eleemnts is nothing but list, where duplicates also allowed.
֍ If we want to represent a group of values as a single entity 
֍ List objects are mutable.i.e we can change the content
֍ List is dynamic because based on our requirement we can increase the size and decrease the size.
֍ In List the elements will be placed within square brackets and with comma seperator. ie. [val,val2] 

'''
print(list_data_type)

crete = '''
Creation of List Objects
1) We can create empty list object as follows...
list = []
print(type(list),list)

O/p : <class 'list'> []

2) With Dynamic Values
list_data = ['Samadhan','26-01-1994',121,True,False,72.982,7276+7737j,6262,"Developer"]
print(type(list_data),list_data)
# ['Samadhan', '26-01-1994', 121, True, False, 72.982, (7276+7737j), 6262, 'Developer']

3) With list() Function:
string = 'SAMADHAN FUKE'
list_data = list(string)
print(type(list_data),list_data)
#OP: <class 'list'> ['S', 'A', 'M', 'A', 'D', 'H', 'A', 'N', ' ', 'F', 'U', 'K', 'E']

4) With split() function 
string = 'Hello , My Name is Samadhan I am From Aurangabad '
list_data = string.split()
print(type(list_data),list_data)
#O/p : <class 'list'> ['Hello', ',', 'My', 'Name', 'is', 'Samadhan', 'I', 'am', 'From', 'Aurangabad']

#Sometimes we can take list inside another list, such type of lists are called nested lists.
nested_list = [1010, '26-01-1994', 121, True, False, 72.982,['OK',7262,False]]
print(type(nested_list),nested_list)
#O/p : <class 'list'> [1010, '26-01-1994', 121, True, False, 72.982, ['OK', 7262, False]]
::: OUTPUT :::
'''
print(crete)

#Create List With Dynamic Values
list_data = ['Samadhan','26-01-1994',121,True,False,72.982,7276+7737j,6262,"Developer"]
print(type(list_data),list_data)
# ['Samadhan', '26-01-1994', 121, True, False, 72.982, (7276+7737j), 6262, 'Developer']
print(list_data[0])
print(list_data[1])

print("NOTE : list is growable mutable in nature. i.e based on our requirement we can increase or decrease the size.")

list_data[0]= 1010
print(list_data)
#[1010, '26-01-1994', 121, True, False, 72.982, (7276+7737j), 6262, 'Developer']

#Create List with list()
string = 'SAMADHAN FUKE'
list_data = list(string)
print(type(list_data),list_data)
#OP: <class 'list'> ['S', 'A', 'M', 'A', 'D', 'H', 'A', 'N', ' ', 'F', 'U', 'K', 'E']

#Create list with split()
string = 'Hello , My Name is Samadhan I am From Aurangabad '
list_data = string.split()
print(type(list_data),list_data)
#O/p : <class 'list'> ['Hello', ',', 'My', 'Name', 'is', 'Samadhan', 'I', 'am', 'From', 'Aurangabad']

#Sometimes we can take list inside another list, such type of lists are called nested lists.
nested_list = [1010, '26-01-1994', 121, True, False, 72.982,['OK',7262,False]]
print(type(nested_list),nested_list)
#O/p : <class 'list'> [1010, '26-01-1994', 121, True, False, 72.982, ['OK', 7262, False]]
print()

tuple_date_type = '''
======================================== TUPLE DATA TYPE =========================================================
1) A Tuple is a collection of Python objects separated by commas
1) Tuple is exactly same as List except that it is immutable 
   i.e once we creates Tuple object, we cannot perform any changes in that object. 
2) Hence Tuple is Read only version of List. 

3) If our data is fixed and never changes then we should go for Tuple.
4) Insertion Order is preserved 
5) Duplicates are allowed 
9) We can represent Tuple elements within Parenthesis and with comma seperator. (obj1,obj1,obj3)
10) Parenethesis are optional but recommended to use. obj,obj2,obj3
'''
print(tuple_date_type)

create_tuple  = '''
 ====================  Creating Tuples  ==================================================
1) Creation of Empty Tuple
Ex: 
tuple_data = ()
print(type(tuple_data),tuple_data)#<class 'tuple'> ()

2) Creation of Single valued Tuple, Parenthesis are Optional, should ends with Comma
Ex:
tuple_data = 10,
tuple_data1 = (20,)
#tuple_data = (10)#<class 'int'> 10
print(type(tuple_data),tuple_data) #<class 'tuple'> (10,)
print(type(tuple_data1),tuple_data1) #<class 'tuple'> (20,)

3) Creation of multi values Tuples & Parenthesis are Optional.
Ex:
tuple_data = 101,"Samadhan","Python","19-07-2021",102
tuple_data1 = (102,"Samadhan","Python","19-07-2021",103)
print(type(tuple_data),tuple_data) #<class 'tuple'> (101, 'Samadhan', 'Python', '19-07-2021', 102)
print(type(tuple_data1),tuple_data1) #<class 'tuple'> (102, 'Samadhan', 'Python', '19-07-2021', 103)

'''
print(create_tuple)

#1) Creation of Empty Tuple

tuple_data = ()
print(type(tuple_data),tuple_data)#<class 'tuple'> ()

#2) Creation of Single valued Tuple, Parenthesis are Optional, should ends with Comma
tuple_data = 10,
tuple_data1 = (20,)
#tuple_data = (10)#<class 'int'> 10
print(type(tuple_data),tuple_data) #<class 'tuple'> (10,)
print(type(tuple_data1),tuple_data1) #<class 'tuple'> (20,)

#3) Creation of multi values Tuples & Parenthesis are Optional.
tuple_data = 101,"Samadhan","Python","19-07-2021",102
tuple_data1 = (102,"Samadhan","Python","19-07-2021",103)

print(type(tuple_data),tuple_data) #<class 'tuple'> (101, 'Samadhan', 'Python', '19-07-2021', 102)
print(type(tuple_data1),tuple_data1) #<class 'tuple'> (102, 'Samadhan', 'Python', '19-07-2021', 103)
print()

set_data_type = '''
===================================== SET DATA TYPE ================================================
 Set is an unordered collection of data type that is iterable, mutable and has no duplicate elements
1 ) If we want to represent a group of unique values as a single entity then we should go for set.
2) Duplicates are not allowed but at the time of set creation, multiple duplicate values can also be passed.
3) Insertion order is not preserved.But we can sort the elements.
4) Indexing and slicing not allowed for the set. 
5) Heterogeneous elements are allowed. 
6) Set objects are mutable i.e once we creates set object we can perform any changes in that object based on our requirement. 
7) We can represent set elements within curly braces and with comma seperation
8) We can apply mathematical operations like union, intersection, difference etc on set objects.
'''
print(set_data_type)

create = '''
 ================== 1) Creating a Set ===================================
Sets can be created:
1) by using the built-in set() function with an iterable object 
Ex: set() Function s = set(any sequence)
set_data = set()
print(type(set_data))#<class 'set'>

# Creating a Set with the use of a List
list = [10,20,30,40,40,"Samadhan","AWS","ASW"]
#List Items Before Set = [10, 20, 30, 40, 40, 'Samadhan', 'AWS', 'ASW']
print("List Items Before Set =",list)
s = set(list)
print(type(s),s)
#<class 'set'> {'Samadhan', 'ASW', 'AWS', 40, 10, 20, 30}  #no duplicate elements

2) a sequence by placing the sequence inside curly braces, separated by ‘comma’
#creation of set by using () inside {}
set_data = {()}
print(type(set_data))##<class 'set'>

set_data = {} #s = {}  It is treated as dictionary but not empty set.
print(type(set_data))##<class 'dict'>
'''
print(create)

#creation of set by using the built-in set() function Ex: set() Function s = set(any sequence)
set_data = set()
print(type(set_data))#<class 'set'>

#creation of set by using () inside {}
set_data = {()}
print(type(set_data))##<class 'set'>

set_data = {} #s = {}  It is treated as dictionary but not empty set.
print(type(set_data))##<class 'dict'>

# Creating a Set with the use of a List
list = [10,20,30,40,40,"Samadhan","AWS","ASW"]
#List Items Before Set = [10, 20, 30, 40, 40, 'Samadhan', 'AWS', 'ASW']
print("List Items Before Set =",list)
s = set(list)
print(type(s),s)
#<class 'set'> {'Samadhan', 'ASW', 'AWS', 40, 10, 20, 30}  #no duplicate elements
print()

#Cloaning of set copy()
#Returns copy of the set. It is cloned object.

set1= {'Samadhan', 'ASW', 'AWS', 40, 10, 20, 30}
print("Set 1 =",id(set1),set1)
set2 = set1.copy()
print("Set 2 is =",id(set2),set2)
print()

dict = '''
================================== Dictionary Data Type ==========================================================
 
Dictionary in Python is an unordered collection of data values
If you want to represent a group of objects as key-value pairs then we should go for Dictionary

֍ Duplicate keys are not allowed but values can be duplicated. 
֍ Hetrogeneous objects are allowed for both key and values.
֍ Insertion order is not preserved
֍ Dictionaries are mutable 
֍ Dictionaries are dynamic 
֍ indexing and slicing concepts are not applicable

Note : Dictionaries have been modified to maintain insertion order with the release of Python 3.7,
so they are now ordered collection of data values.
'''
print(dict)

create = '''
===================== Creating a Dictionary =====================================
Dictionary can be created by placing a sequence of elements within curly {} braces, separated by ‘comma’. 
Dictionary holds a pair of values, one being the Key and the other corresponding pair element being its Key:value.
Values in a dictionary can be of any data type and can be duplicated, whereas keys can’t be repeated and must be immutable. 

Note – Dictionary keys are case sensitive, the same name but different cases of Key will be treated distinctly. 

# Creating Empty Dictionary 
dict_data  = {}
print(type(dict_data))
#<class 'dict'>

# Creating Empty Dictionary with dict() function
dict_data = dict()
print(type(dict_data))
#<class 'dict'> 

# Creating  Dictionary with key: value data if we know the data already 
dict1 = {"Name":"Samadhan","Age":28,"City":"Pune",121 :"ID"}
print("Data from dict1",dict1)
#{'Name': 'Samadhan', 'Age': 28, 'City': 'Pune', 121: 'ID'}

# Creating  Dictionary with key: value data
dict2 = dict({"Name":"Samadhan","Age":28,"City":"Pune",121 :"ID"})
print("Data from dict2", dict2)
#{'Name': 'Samadhan', 'Age': 28, 'City': 'Pune', 121: 'ID'}

# Creating a Dictionary with Integer Keys
dict_data = {1:"FirstName",1_1:"FirstName",2:"LastName",3:"City",4:"Contact",5:5,5:5}
print(type(dict_data))
print(dict_data)
#{1: 'FirstName', 11: 'FirstName', 2: 'LastName', 3: 'City', 4: 'Contact', 5: 5}

#Duplicate keys are not allowed but values can be duplicated
dict_data = {1:"FirstName",1:"FisrtName",1_1:"FirstName",2:"LastName",3:"City",4:"Contact",5:5,5:5}
print(dict_data)#{1: 'FisrtName', 11: 'FirstName', 2: 'LastName', 3: 'City', 4: 'Contact', 5: 5}

#Nested Dictionary:
dict_data = {1:"FirstName",2:"LastName",3:{"ID":101,"User":"Samadhan"},4:"Nested Dictionary Example"}
print(dict_data) #{1: 'FirstName', 2: 'LastName', 3: {'ID': 101, 'User': 'Samadhan'}, 4: 'Nested Dictionary Example'}

'''
print(create)

# Creating Empty Dictionary 
dict_data  = {}
print(type(dict_data))
#<class 'dict'>

# Creating Empty Dictionary with dict() function
dict_data = dict()
print(type(dict_data))
#<class 'dict'> 

# Creating  Dictionary with key: value data if we know the data already 
dict1 = {"Name":"Samadhan","Age":28,"City":"Pune",121 :"ID"}
print("Data from dict1",dict1)
#{'Name': 'Samadhan', 'Age': 28, 'City': 'Pune', 121: 'ID'}

# Creating  Dictionary with key: value data
dict2 = dict({"Name":"Samadhan","Age":28,"City":"Pune",121 :"ID"})
print("Data from dict2", dict2)
#{'Name': 'Samadhan', 'Age': 28, 'City': 'Pune', 121: 'ID'}

# Creating a Dictionary with Integer Keys
dict_data = {1:"FirstName",1_1:"FirstName",2:"LastName",3:"City",4:"Contact",5:5,5:5}
print(type(dict_data))
print(dict_data)
#{1: 'FirstName', 11: 'FirstName', 2: 'LastName', 3: 'City', 4: 'Contact', 5: 5}

#Duplicate keys are not allowed but values can be duplicated
dict_data = {1:"FirstName",1:"FisrtName",1_1:"FirstName",2:"LastName",3:"City",4:"Contact",5:5,5:5}
print(dict_data)#{1: 'FisrtName', 11: 'FirstName', 2: 'LastName', 3: 'City', 4: 'Contact', 5: 5}

#Nested Dictionary:
dict_data = {1:"FirstName",2:"LastName",3:{"ID":101,"User":"Samadhan"},4:"Nested Dictionary Example"}
print(dict_data) #{1: 'FirstName', 2: 'LastName', 3: {'ID': 101, 'User': 'Samadhan'}, 4: 'Nested Dictionary Example'}
